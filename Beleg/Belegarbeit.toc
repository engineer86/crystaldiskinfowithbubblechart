\babel@toc {german}{}
\contentsline {section}{\numberline {1}Einf\IeC {\"u}hrung}{4}{section.1}
\contentsline {subsection}{\numberline {1.1}Aufgabenstellung}{4}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Beschreibung der Software}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Visionen und Ziele des Originalprojektes}{5}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Programm vor \IeC {\"A}nderung}{5}{subsection.1.4}
\contentsline {section}{\numberline {2}Aufbau des Programmes}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Programmiersprache(n)}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}\IeC {\"U}bersicht des Quellcodes}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Ordnerstruktur}{8}{subsection.2.3}
\contentsline {section}{\numberline {3}Modellierung}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}Use-Case-Modell}{9}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}anzeigenDatentr\IeC {\"a}gerInformation}{10}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}einstellenRateAktualisierung}{10}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}anzeigenGraph}{10}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}einstellenBenachrichtigung}{11}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}benachrichtigenNutzer}{11}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}auswahlTheme}{11}{subsubsection.3.1.6}
\contentsline {subsubsection}{\numberline {3.1.7}auswahlSprache}{11}{subsubsection.3.1.7}
\contentsline {subsection}{\numberline {3.2}Dom\IeC {\"a}nenmodell}{12}{subsection.3.2}
\contentsline {section}{\numberline {4}Geforderte \IeC {\"A}nderungen}{12}{section.4}
\contentsline {subsection}{\numberline {4.1}Visionen und Ziele der \IeC {\"A}nderung}{12}{subsection.4.1}
\contentsline {section}{\numberline {5}Modellierung nach den \IeC {\"A}nderungen}{13}{section.5}
\contentsline {subsection}{\numberline {5.1}Use-Case-Modell}{13}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Neue Klasse}{13}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Dom\IeC {\"a}nenmodell}{15}{subsection.5.3}
\contentsline {section}{\numberline {6}Beschreibung der Implementierung}{15}{section.6}
\contentsline {section}{\numberline {7}Programm nach \IeC {\"A}nderung}{15}{section.7}
\contentsline {section}{\numberline {8}Fazit}{17}{section.8}
